require "rails_helper"

RSpec.describe ResultadosLaboratoriosController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/resultados_laboratorios").to route_to("resultados_laboratorios#index")
    end


    it "routes to #show" do
      expect(:get => "/resultados_laboratorios/1").to route_to("resultados_laboratorios#show", :id => "1")
    end


    it "routes to #create" do
      expect(:post => "/resultados_laboratorios").to route_to("resultados_laboratorios#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/resultados_laboratorios/1").to route_to("resultados_laboratorios#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/resultados_laboratorios/1").to route_to("resultados_laboratorios#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/resultados_laboratorios/1").to route_to("resultados_laboratorios#destroy", :id => "1")
    end

  end
end
