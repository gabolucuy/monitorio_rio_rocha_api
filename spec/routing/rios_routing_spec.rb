require "rails_helper"

RSpec.describe RiosController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/rios").to route_to("rios#index")
    end

    it "routes to #show" do
      expect(:get => "/rios/1").to route_to("rios#show", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/rios").to route_to("rios#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/rios/1").to route_to("rios#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/rios/1").to route_to("rios#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/rios/1").to route_to("rios#destroy", :id => "1")
    end

  end
end
