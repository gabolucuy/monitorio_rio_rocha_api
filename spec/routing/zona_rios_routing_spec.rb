require "rails_helper"

RSpec.describe ZonaRiosController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/zona_rios").to route_to("zona_rios#index")
    end

    it "routes to #show" do
      expect(:get => "/zona_rios/1").to route_to("zona_rios#show", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/zona_rios").to route_to("zona_rios#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/zona_rios/1").to route_to("zona_rios#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/zona_rios/1").to route_to("zona_rios#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/zona_rios/1").to route_to("zona_rios#destroy", :id => "1")
    end

  end
end
