require "rails_helper"

RSpec.describe InstitucionsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/institucions").to route_to("institucions#index")
    end

    it "routes to #show" do
      expect(:get => "/institucions/1").to route_to("institucions#show", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/institucions").to route_to("institucions#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/institucions/1").to route_to("institucions#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/institucions/1").to route_to("institucions#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/institucions/1").to route_to("institucions#destroy", :id => "1")
    end

  end
end
