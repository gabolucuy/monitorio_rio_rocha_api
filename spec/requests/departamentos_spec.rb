require 'rails_helper'

RSpec.describe "Departamentos", type: :request do
  describe "When all departamentos are required" do
    let!(:departametos) {create_list(:departamento,10)}
    let(:user) { create(:user,departamento: departametos.first) }
    let(:headers) { valid_headers }
    before { get '/departamentos', params: {}, headers: headers}
    it "return all departametos" do
      expect(json).not_to be_empty
      expect(json.size).to eq(10)
      expect(response).to have_http_status(200)
    end
  end
  describe "When a new department will be added" do
    context 'when the request is valid' do
      let(:valid_attributes) { {nombre:"Cochabamba"} }
      let(:user) { create(:user) }
      let(:headers) { valid_headers }
      before { post '/departamentos',params: valid_attributes.to_json, headers: headers }
      it "creates a new record" do
        expect(json['nombre']).to eq(valid_attributes[:nombre])
      end
      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end
    end
  end
end
