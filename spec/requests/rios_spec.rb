require 'rails_helper'

RSpec.describe "Rios", type: :request do
  let!(:rios) {create_list(:rio,10)}
  let(:user) { create(:user) }
  let(:headers) { valid_headers }
  describe "when all the rivers are required" do
    before { get '/rios', params: {}, headers: headers }
    it "returns all the rivers" do
      expect(json).not_to be_empty
      expect(json.size).to eq(10)
      expect(response).to have_http_status(200)
    end
  end
  describe "when a new record will be added" do
    let(:departamento) {create :departamento}
    let(:valid_attributes) {{ nombre:"Rio rocha", cantidad_zonas:8, departamento_id: departamento.id}}
    context "when the request is valid" do
      before { post '/rios',params: valid_attributes.to_json, headers: headers }
      it "creates the new record" do
        expect(json['nombre']).to eq(valid_attributes[:nombre])
        expect(json['cantidad_zonas']).to eq(valid_attributes[:cantidad_zonas])
        expect(json['departamento_id']).to eq(valid_attributes[:departamento_id])
      end
      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end
    end
  end

end
