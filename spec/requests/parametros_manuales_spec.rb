require 'rails_helper'

RSpec.describe 'Paramatros manuales', type: :request do
  let!(:parametros_manuales) { create_list(:parametros_manuale, 10) }
  let!(:user) { create(:user) }
  let!(:headers) { valid_headers }
  describe 'when all the parametros manuales are required' do
    before { get '/parametros_manuales', params: {}, headers: headers }

    it 'returns all the data created' do
      # Note `json` is a custom helper to parse JSON responses
      expect(json).not_to be_empty
      expect(json.size).to eq(10)
    end

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end

  end

  describe 'when a new lecture will be added' do
    # valid payload
    let!(:zona_rio) {create :zona_rio}
    let(:valid_attributes) { { fosfato:1,nitrito:2,nitrato:1.1,ph:1.5,amonio:1.8,alcalinidad:1,dureza_total:1,cloruro:1,oxigeno_disuelto:1,temperatura:1,conductividad:1,user_id:user.id,zona_rio_id:zona_rio.id} }

    context 'when the request is valid' do
      before { post '/parametros_manuales',params: valid_attributes.to_json, headers: headers }

      it 'creates a parametro manual' do
        expect(json['fosfato']).to eq(valid_attributes[:fosfato])
        expect(json['nitrato']).to eq(valid_attributes[:nitrato])
      end

      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end
    end
  end

end
