require 'rails_helper'

RSpec.describe "ZonaRios", type: :request do
  let!(:zona_rios) {create_list(:zona_rio,10)}
  let(:user) { create(:user) }
  let(:headers) { valid_headers }
  describe "when all the rivers zones are required " do
    before { get '/zona_rios', params: {}, headers: headers }
    it "return all the rivers zones" do
      expect(json).not_to be_empty
      expect(json.size).to eq(10)
      expect(response).to have_http_status(200)
    end
  end

  describe "when a new record will be added" do
    let(:rio) {create :rio}
    let(:valid_attributes) {{latitud:11.9,longitud:-74.5,rio_id:rio.id}}
    context "when the request is valid" do
      before { post '/zona_rios',params: valid_attributes.to_json, headers: headers }
      it "creates the new record" do
        expect(json['latitud']).to eq(valid_attributes[:latitud])
        expect(json['longitud']).to eq(valid_attributes[:longitud])
        expect(json['rio_id']).to eq(valid_attributes[:rio_id])
      end
      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end
    end
  end
end
