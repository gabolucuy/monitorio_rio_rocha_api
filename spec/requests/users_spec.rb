require 'rails_helper'

RSpec.describe "Users", type: :request do
  let!(:departamento) {create :departamento}
  let!(:institucion) {create :institucion}
  let(:user) { create(:user) }

  let(:headers) { valid_headers.except('Authorization') }
  let(:valid_attributes) do
    attributes_for(:user, departamento_id:departamento.id, institucion_id:institucion.id, password_confirmation: user.password,pin:institucion.pin)
  end
  # let(:valid_attributes) {{nombre:"gabo",apellido:"lucuy",ci:4578,email:"gabo.com",password:"gabo",password_confirmation:"gabo",departamento_id:departamento.id,institucion_id:institucion.id}}


  describe 'POST /signup' do
    context 'when valid request' do


      before { post '/signup', params: valid_attributes.to_json, headers: headers }

      it 'creates a new user' do
        expect(response).to have_http_status(201)
      end

      it 'returns success message' do
        expect(json['auth_token']).to be_present
      end

      it 'returns an authentication token' do
        expect(json['auth_token']).not_to be_nil
      end
    end

    context 'when invalid request but pin is correct' do
      let!(:institucion1) {create :institucion}
      let!(:valid_attributes1) { {pin:institucion.pin ,institucion_id:institucion.id} }
      before { post '/signup', params: valid_attributes1.to_json, headers: headers }

      it 'does not create a new user' do
        expect(response).to have_http_status(422)
      end

      it 'returns failure message' do
        expect(json['email'].to_s).to match("Correo es obligatorio")
      end
    end
    context 'when the pin is invalid in the request' do
      let!(:institucion1) {create :institucion}
      let!(:valid_attributes1) { {institucion_id:institucion.id} }
      before { post '/signup', params: valid_attributes1.to_json, headers: headers }

      it 'does not create a new user' do
        expect(response).to have_http_status(401)
      end

      it 'returns failure message' do
        expect(json['message'].to_s).to match("Pin incorrecto")
      end
    end
  end
end
