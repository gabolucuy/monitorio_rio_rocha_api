require 'rails_helper'

RSpec.describe "Institucions", type: :request do
  let!(:institucions) {create_list(:institucion,10)}
  let(:user) { create(:user,institucion: institucions.first) }
  let(:headers) { valid_headers }

  describe "when all the institucions are required " do
    before { get '/institucions', params: {}, headers: headers }
    it "returns all the institucions" do
      expect(json).not_to be_empty
      expect(json.size).to eq(10)
      expect(response).to have_http_status(200)
    end
  end
  describe "when a new record will be added" do
    let!(:valid_attributes) {{ nombre: "CBA", direccion:"Av.America", telefono:4242465 }}
    let(:user) { create(:user) }
    let(:headers) { valid_headers }
    context "when the request is valid" do
      before { post '/institucions',params:valid_attributes.to_json, headers: headers}
      it "creates the record" do
        expect(json['nombre']).to eq(valid_attributes[:nombre])
        expect(json['direccion']).to eq(valid_attributes[:direccion])
        expect(json['telefono'].to_i).to eq(valid_attributes[:telefono])
      end
      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end
      it "creates automatically a pin for the institucion" do
        pin = Institucion.find(json['id'].to_i).pin
        expect(json['pin'].to_i).to eq(pin)
      end
    end
  end
end
