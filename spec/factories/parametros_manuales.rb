FactoryBot.define do
  factory :parametros_manuale do
    fosfato { Faker::Number.number(2) }
    nitrito { Faker::Number.number(2) }
    nitrato { Faker::Number.number(2) }
    ph { Faker::Number.number(2) }
    amonio { Faker::Number.number(2) }
    alcalinidad { Faker::Number.number(2) }
    dureza_total { Faker::Number.number(2) }
    cloruro { Faker::Number.number(2) }
    oxigeno_disuelto { Faker::Number.number(2) }
    temperatura { Faker::Number.number(2) }
    conductividad { Faker::Number.number(2) }
    zona_rio {create :zona_rio}
    user {create :user}
  end
end
