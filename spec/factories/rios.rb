FactoryBot.define do
  factory :rio do
    nombre { Faker::Name.first_name   }
    cantidad_zonas { Faker::Number.number(1) }
    departamento { create :departamento }

  end
end
