FactoryBot.define do
  factory :departamento do
    nombre { Faker::Address.city }
  end
end
