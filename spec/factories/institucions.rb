FactoryBot.define do
  factory :institucion do
    nombre { Faker::GameOfThrones.character   }
    direccion { Faker::Address.street_name   }
    telefono { Faker::Number.number(7) }
    pin { Faker::Number.number(4) }
  end
end
