FactoryBot.define do
  factory :user do
    nombre { Faker::Name.first_name }
    apellido { Faker::Name.last_name }
    ci { Faker::Number.number(8)}
    email { Faker::Internet.email }
    password { 'rocha'}
    departamento { create :departamento }
    institucion { create :institucion }
  end

  factory :simpler_user, :class => 'User' do
    nombre { Faker::Name.first_name }
    apellido { Faker::Name.last_name }
    ci { Faker::Number.number(8)}
    email { Faker::Internet.email }
    password { 'rocha'}
	end
end
