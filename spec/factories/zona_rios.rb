FactoryBot.define do
  factory :zona_rio do
    latitud { Faker::Address.latitude   }
    longitud { Faker::Address.longitude }
    rio { create :rio }
  end
end
