require 'rails_helper'

RSpec.describe User, type: :model do
  it { should belong_to(:departamento) }
  it { should belong_to(:institucion) }
  it { should have_many(:parametros_manuales) }

  it { should validate_uniqueness_of(:email).with_message(/Ya existe una cuenta asociada a este correo/) }
  it { should validate_presence_of(:nombre).with_message(/Nombre es obligatorio/) }
  it { should validate_presence_of(:apellido).with_message(/Apellido es obligatorio/) }
  it { should validate_presence_of(:ci).with_message(/CI es obligatorio/) }
  it { should validate_presence_of(:email).with_message(/Correo es obligatorio/) }
  it { should validate_presence_of(:password_digest).with_message(/Contraseña es obligatorio/) }
  it { should validate_presence_of(:departamento_id).with_message(/departamento_id es obligatorio/) }
  it { should validate_presence_of(:institucion_id).with_message(/institucion_id es obligatorio/) }
end
