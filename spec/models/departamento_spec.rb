require 'rails_helper'

RSpec.describe Departamento, type: :model do
  it { should have_many(:users) }

  it { should validate_presence_of(:nombre).with_message(/Nombre del departamento es obligatorio/) }
  it { should have_many(:rios) }

end
