require 'rails_helper'

RSpec.describe Institucion, type: :model do
  it { should have_many(:users) }

  it { should validate_presence_of(:nombre).with_message(/Nombrede la institucion es obligatorio/) }
  it { should validate_presence_of(:direccion).with_message(/Direccion es obligatorio/) }
  it { should validate_presence_of(:telefono).with_message(/Telefono es obligatorio/) }

end
