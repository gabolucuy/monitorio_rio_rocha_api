require 'rails_helper'

RSpec.describe ParametrosManuale, type: :model do
  it { should belong_to(:user) }
  it { should belong_to(:zona_rio) }

  it { should validate_presence_of(:fosfato).with_message(/Fosfato se tiene que llenar/) }
  it { should validate_presence_of(:nitrito).with_message(/Nitrito se tiene que llenar/) }
  it { should validate_presence_of(:nitrato).with_message(/Nitrato se tiene que llenar/) }
  it { should validate_presence_of(:ph).with_message(/Ph se tiene que llenar/) }
  it { should validate_presence_of(:amonio).with_message(/Amonio se tiene que llenar/) }
  it { should validate_presence_of(:alcalinidad).with_message(/Alcalinidad se tiene que llenar/) }
  it { should validate_presence_of(:dureza_total).with_message(/Dureza_total se tiene que llenar/) }
  it { should validate_presence_of(:cloruro).with_message(/Cloruro se tiene que llenar/) }
  it { should validate_presence_of(:oxigeno_disuelto).with_message(/Oxigeno_disuelto se tiene que llenar/) }
  it { should validate_presence_of(:temperatura).with_message(/Temperatura se tiene que llenar/) }
  it { should validate_presence_of(:conductividad).with_message(/Conductividad se tiene que llenar/) }
  it { should validate_presence_of(:zona_rio_id).with_message(/Zona rio se tiene que llenar/) }


end
