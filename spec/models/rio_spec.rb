require 'rails_helper'

RSpec.describe Rio, type: :model do
  it { should belong_to(:departamento) }
  it { should have_many(:zona_rios) }

  it { should validate_presence_of(:nombre).with_message(/Nombre del rio es obligatorio/) }
  it { should validate_presence_of(:cantidad_zonas).with_message(/Cantidad de zonas es obligatorio/) }
  it { should validate_presence_of(:departamento_id).with_message(/Departamento es obligatorio/) }

end
