require 'rails_helper'

RSpec.describe ZonaRio, type: :model do
  it { should belong_to(:rio) }
  it { should have_many(:parametros_manuales) }

  it { should validate_presence_of(:latitud).with_message(/Latitud es obligatori/) }
  it { should validate_presence_of(:longitud).with_message(/Longitud es obligatorio/) }
  it { should validate_presence_of(:rio_id).with_message(/Rio es obligatorio/) }
end
