Rails.application.routes.draw do
  resources :resultados_laboratorios do
    collection do
      get 'last_of_each_zone'
    end
  end
  resources :users
  resources :institucions
  resources :zona_rios
  resources :rios
  resources :departamentos
  resources :parametros_manuales do
    collection do
      get 'last_of_each_zone'
    end
  end
  post 'auth/login', to: 'authentication#authenticate'
  post 'signup', to: 'users#create'
end
