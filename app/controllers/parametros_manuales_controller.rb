class ParametrosManualesController < ApplicationController
  skip_before_action :authorize_request, only: [:index,:last_of_each_zone]
  before_action :set_parametros_manuale, only: [:show, :update, :destroy]

  def index
    @parametros_manuales = ParametrosManuale.all
    render json: @parametros_manuales
  end

  def create
    @parametros_manuale = ParametrosManuale.new(parametros_manuales_params)
    if @parametros_manuale.save
      render json: @parametros_manuale, status: :created, location: @parametros_manuale
    else
      render json: @parametros_manuale.errors, status: :unprocessable_entity
    end
  end

  def last_of_each_zone
    last_params = []
    zonas_rios = ZonaRio.all
    zonas_rios.each do |zona|
      last_params[zona.id] = zona.parametros_manuales.last
    end
    render json: last_params
  end

  private

  def parametros_manuales_params
    params.permit(:fosfato,:nitrito,:nitrato,:ph,:amonio,:alcalinidad,:dureza_total,:oxigeno_disuelto,:temperatura,:conductividad,:user_id,:zona_rio_id)
  end

  def set_parametros_manuale
    @parametros_manuale = ParametrosManuale.find(params[:id])
  end

end
