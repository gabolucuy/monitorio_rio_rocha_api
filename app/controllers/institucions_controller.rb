class InstitucionsController < ApplicationController
  skip_before_action :authorize_request, only: :index
  before_action :set_institucion, only: [:show, :update, :destroy]
  # GET /institucions
  def index
    @institucions = Institucion.all

    render json: @institucions
  end

  # GET /institucions/1
  def show
    render json: @institucion
  end

  # POST /institucions
  def create
    @institucion = Institucion.new(institucion_params)

    if @institucion.save
      render json: @institucion, status: :created, location: @institucion
    else
      render json: @institucion.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /institucions/1
  def update
    if @institucion.update(institucion_params)
      render json: @institucion
    else
      render json: @institucion.errors, status: :unprocessable_entity
    end
  end

  # DELETE /institucions/1
  def destroy
    @institucion.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_institucion
      @institucion = Institucion.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def institucion_params
      params.permit(:nombre, :direccion, :telefono, :pin)
    end

end
