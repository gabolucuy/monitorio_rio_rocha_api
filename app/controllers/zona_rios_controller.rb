class ZonaRiosController < ApplicationController
  skip_before_action :authorize_request, only: :index
  before_action :set_zona_rio, only: [:show, :update, :destroy]

  # GET /zona_rios
  def index
    @zona_rios = ZonaRio.all

    render json: @zona_rios
  end

  # GET /zona_rios/1
  def show
    render json: @zona_rio
  end

  # POST /zona_rios
  def create
    @zona_rio = ZonaRio.new(zona_rio_params)

    if @zona_rio.save
      render json: @zona_rio, status: :created, location: @zona_rio
    else
      render json: @zona_rio.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /zona_rios/1
  def update
    if @zona_rio.update(zona_rio_params)
      render json: @zona_rio
    else
      render json: @zona_rio.errors, status: :unprocessable_entity
    end
  end

  # DELETE /zona_rios/1
  def destroy
    @zona_rio.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_zona_rio
      @zona_rio = ZonaRio.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def zona_rio_params
      params.permit(:latitud, :longitud, :rio_id)
    end
end
