class RiosController < ApplicationController
  before_action :set_rio, only: [:show, :update, :destroy]

  # GET /rios
  def index
    @rios = Rio.all

    render json: @rios
  end

  # GET /rios/1
  def show
    render json: @rio
  end

  # POST /rios
  def create
    @rio = Rio.new(rio_params)

    if @rio.save
      render json: @rio, status: :created, location: @rio
    else
      render json: @rio.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /rios/1
  def update
    if @rio.update(rio_params)
      render json: @rio
    else
      render json: @rio.errors, status: :unprocessable_entity
    end
  end

  # DELETE /rios/1
  def destroy
    @rio.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_rio
      @rio = Rio.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def rio_params
      params.permit(:nombre, :cantidad_zonas, :departamento_id)
    end
end
