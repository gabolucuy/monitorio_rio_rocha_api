class ResultadosLaboratoriosController < ApplicationController
  skip_before_action :authorize_request, only: [:index,:last_of_each_zone]
  before_action :set_resultados_laboratorio, only: [:show, :update, :destroy]

  # GET /resultados_laboratorios
  def index
    @resultados_laboratorios = ResultadosLaboratorio.all

    render json: @resultados_laboratorios
  end

  # GET /resultados_laboratorios/1
  def show
    render json: @resultados_laboratorio
  end

  # POST /resultados_laboratorios
  def create
    @resultados_laboratorio = ResultadosLaboratorio.new(resultados_laboratorio_params)
    if @resultados_laboratorio.save
      render json: @resultados_laboratorio, status: :created, location: @resultados_laboratorio
    else
      render json: @resultados_laboratorio.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /resultados_laboratorios/1
  def update
    if @resultados_laboratorio.update(resultados_laboratorio_params)
      render json: @resultados_laboratorio
    else
      render json: @resultados_laboratorio.errors, status: :unprocessable_entity
    end
  end

  def last_of_each_zone
    last_params = []
    zonas_rios = ZonaRio.all
    zonas_rios.each do |zona|
      last_params[zona.id] = zona.resultados_laboratorios.last
    end
    last_params[0] = {ico:ResultadosLaboratorio.all.average(:ico).to_f}
    render json: last_params
  end

  # DELETE /resultados_laboratorios/1
  def destroy
    @resultados_laboratorio.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_resultados_laboratorio
      @resultados_laboratorio = ResultadosLaboratorio.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def resultados_laboratorio_params
      params.permit(:DBO5_lab, :OD_lab, :NH3_lab, :DBO5, :OD, :NH3, :ico, :zona_rio_id)
    end
end
