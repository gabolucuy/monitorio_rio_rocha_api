class UsersController < ApplicationController
  skip_before_action :authorize_request, only: :create
  before_action :set_user, only: [:show, :update, :destroy]

  # GET /users
  def index
    @users = User.all

    render json: @users
  end

  # GET /users/1
  def show
    render json: @user
  end

  # POST /users
  def create
    institucion = Institucion.find(params[:institucion_id])
    if institucion.pin.to_i == pin_param['pin'].to_i
    @user = User.new(user_params)
      if @user.save
        auth_token = AuthenticateUser.new(@user.email, @user.password).call
        render json: {user:@user,auth_token: auth_token}, status: :created, location: @user
      else
        render json: @user.errors, status: :unprocessable_entity
      end
    else
      render json: {message:"Pin incorrecto"}, status: :unauthorized
    end
  end

  # PATCH/PUT /users/1
  def update
    if @user.update(user_params)
      render json: @user
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  # DELETE /users/1
  def destroy
    @user.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def user_params
      params.permit(:nombre, :apellido, :ci, :email, :password,:password_confirmation, :departamento_id, :institucion_id)
    end
    def pin_param
      params.permit(:pin)
    end
end
