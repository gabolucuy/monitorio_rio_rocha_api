class ParametrosManuale < ApplicationRecord
  belongs_to :user
  belongs_to :zona_rio

  validates_presence_of :fosfato,:message => "Fosfato se tiene que llenar"
  validates_presence_of :nitrito,:message => "Nitrito se tiene que llenar"
  validates_presence_of :nitrato,:message => "Nitrato se tiene que llenar"
  validates_presence_of :ph,:message => "Ph se tiene que llenar"
  validates_presence_of :amonio,:message => "Amonio se tiene que llenar"
  validates_presence_of :alcalinidad,:message => "Alcalinidad se tiene que llenar"
  validates_presence_of :dureza_total,:message => "Dureza_total se tiene que llenar"
  validates_presence_of :zona_rio_id,:message => "Zona rio se tiene que llenar"
end
