class User < ApplicationRecord
  has_secure_password
  belongs_to :departamento
  belongs_to :institucion
  has_many :parametros_manuales
  validates_uniqueness_of :email,:message => "Ya existe una cuenta asociada a este correo"
  validates_presence_of :nombre,:message => "Nombre es obligatorio"
  validates_presence_of :apellido,:message => "Apellido es obligatorio"
  validates_presence_of :ci,:message => "CI es obligatorio"
  validates_presence_of :email,:message => "Correo es obligatorio"
  validates_presence_of :password_digest,:message => "Contraseña es obligatorio"
  validates_presence_of :departamento_id,:message => "departamento_id es obligatorio"
  validates_presence_of :institucion_id,:message => "institucion_id es obligatorio"
end
