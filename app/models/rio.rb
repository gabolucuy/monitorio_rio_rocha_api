class Rio < ApplicationRecord
  belongs_to :departamento
  has_many :zona_rios
  validates_presence_of :nombre, :message => "Nombre del rio es obligatorio"
  validates_presence_of :cantidad_zonas, :message => "Cantidad de zonas es obligatorio"
  validates_presence_of :departamento_id, :message => "Departamento es obligatorio"

end
