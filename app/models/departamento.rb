class Departamento < ApplicationRecord
  has_many :rios
  has_many :users
  validates_presence_of :nombre, :message => "Nombre del departamento es obligatorio"
end
