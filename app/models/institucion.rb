class Institucion < ApplicationRecord
  before_create :generate_pin

  has_many :users
  validates_presence_of :nombre, :message => "Nombrede la institucion es obligatorio"
  validates_presence_of :direccion, :message => "Direccion es obligatorio"
  validates_presence_of :telefono, :message => "Telefono es obligatorio"

  private
  def generate_pin
    self.pin = 4.times.map { rand(1..9) }.join.to_i;
    generate_pin if Institucion.exists?(pin: self.pin)
  end

end
