class ResultadosLaboratorio < ApplicationRecord
  belongs_to :zona_rio
  after_create :calculate_params

def calculate_params
  dbo5 = self.DBO5_lab / 1.5
  if self.OD_lab >= 0 && self.OD_lab <= 50
    od = 4.2 - 0.437 * ( (100 - self.OD_lab) / 5 ) + 0.042 * ( (100 - self.OD_lab) / 5 ) ** 2
  end
  if self.OD_lab >=51 && self.OD_lab <=100
    od = 0.08 * (100 - self.OD_lab)
  end
  if  self.OD_lab > 100
    od = 0.08 * (self.OD_lab - 100)
  end
  nh3 = 2 ** (2.1 * Math::log( (10*self.NH3_lab),10 ))
  self.update_column(:DBO5 ,dbo5.round(5))
  self.update_column(:OD ,od.round(5))
  self.update_column(:NH3 ,nh3.round(5))
  self.update_column(:ico ,((dbo5+od+nh3)/3).round(5))
end

end
