class ZonaRio < ApplicationRecord
  belongs_to :rio
  has_many :resultados_laboratorios
  has_many :parametros_manuales

  validates_presence_of :latitud, :message => "Latitud es obligatorio"
  validates_presence_of :longitud, :message => "Longitud es obligatorio"
  validates_presence_of :rio_id, :message => "Rio es obligatorio"

end
