# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_10_17_043502) do

  create_table "departamentos", force: :cascade do |t|
    t.string "nombre"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "institucions", force: :cascade do |t|
    t.string "nombre"
    t.string "direccion"
    t.string "telefono"
    t.integer "pin"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "parametros_manuales", force: :cascade do |t|
    t.float "fosfato"
    t.float "nitrito"
    t.float "nitrato"
    t.float "ph"
    t.float "amonio"
    t.float "alcalinidad"
    t.float "dureza_total"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "zona_rio_id"
    t.integer "user_id"
    t.index ["user_id"], name: "index_parametros_manuales_on_user_id"
    t.index ["zona_rio_id"], name: "index_parametros_manuales_on_zona_rio_id"
  end

  create_table "resultados_laboratorios", force: :cascade do |t|
    t.float "DBO5_lab"
    t.float "OD_lab"
    t.float "NH3_lab"
    t.float "DBO5"
    t.float "OD"
    t.float "NH3"
    t.float "ico"
    t.integer "zona_rio_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["zona_rio_id"], name: "index_resultados_laboratorios_on_zona_rio_id"
  end

  create_table "rios", force: :cascade do |t|
    t.string "nombre"
    t.integer "cantidad_zonas"
    t.integer "departamento_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["departamento_id"], name: "index_rios_on_departamento_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "nombre"
    t.string "apellido"
    t.string "ci"
    t.string "email"
    t.string "password_digest"
    t.integer "departamento_id"
    t.integer "institucion_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["departamento_id"], name: "index_users_on_departamento_id"
    t.index ["institucion_id"], name: "index_users_on_institucion_id"
  end

  create_table "zona_rios", force: :cascade do |t|
    t.float "latitud"
    t.float "longitud"
    t.integer "rio_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["rio_id"], name: "index_zona_rios_on_rio_id"
  end

end
