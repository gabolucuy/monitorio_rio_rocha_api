class CreateResultadosLaboratorios < ActiveRecord::Migration[5.2]
  def change
    create_table :resultados_laboratorios do |t|
      t.float :DBO5_lab
      t.float :OD_lab
      t.float :NH3_lab
      t.float :DBO5
      t.float :OD
      t.float :NH3
      t.float :ico
      t.references :zona_rio, foreign_key: true

      t.timestamps
    end
  end
end
