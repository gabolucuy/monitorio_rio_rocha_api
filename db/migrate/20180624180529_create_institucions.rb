class CreateInstitucions < ActiveRecord::Migration[5.2]
  def change
    create_table :institucions do |t|
      t.string :nombre
      t.string :direccion
      t.string :telefono
      t.integer :pin

      t.timestamps
    end
  end
end
