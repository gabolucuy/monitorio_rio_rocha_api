class CreateRios < ActiveRecord::Migration[5.2]
  def change
    create_table :rios do |t|
      t.string :nombre
      t.integer :cantidad_zonas
      t.references :departamento, foreign_key: true

      t.timestamps
    end
  end
end
