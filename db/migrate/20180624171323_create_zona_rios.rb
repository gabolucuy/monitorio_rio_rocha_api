class CreateZonaRios < ActiveRecord::Migration[5.2]
  def change
    create_table :zona_rios do |t|
      t.float :latitud
      t.float :longitud
      t.references :rio, foreign_key: true

      t.timestamps
    end
  end
end
