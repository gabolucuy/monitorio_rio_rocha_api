class AddReferencesToParametrosManuales < ActiveRecord::Migration[5.2]
  def change
    add_reference :parametros_manuales, :zona_rio, foreign_key: true
    add_reference :parametros_manuales, :user, foreign_key: true
  end
end
