class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :nombre
      t.string :apellido
      t.string :ci
      t.string :email
      t.string :password_digest
      t.references :departamento, foreign_key: true
      t.references :institucion, foreign_key: true

      t.timestamps
    end
  end
end
