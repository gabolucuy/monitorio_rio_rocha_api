class CreateParametrosManuales < ActiveRecord::Migration[5.1]
  def change
    create_table :parametros_manuales do |t|
      t.float :fosfato
      t.float :nitrito
      t.float :nitrato
      t.float :ph
      t.float :amonio
      t.float :alcalinidad
      t.float :dureza_total
      t.float :cloruro
      t.float :oxigeno_disuelto
      t.float :temperatura
      t.float :conductividad

      t.timestamps
    end
  end
end
