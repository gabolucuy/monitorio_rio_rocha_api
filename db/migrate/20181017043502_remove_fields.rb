class RemoveFields < ActiveRecord::Migration[5.2]
  def change
    remove_column :parametros_manuales, :cloruro
    remove_column :parametros_manuales, :oxigeno_disuelto
    remove_column :parametros_manuales, :temperatura
    remove_column :parametros_manuales, :conductividad
  end
end
