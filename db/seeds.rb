# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
Departamento.create(nombre:"Cochabamba")

Institucion.create(nombre:"7ma división",direccion:"Av. America",telefono:4242454)
Institucion.create(nombre:"EMI",direccion:"Av. America",telefono:4242487)
Institucion.create(nombre:"CBA",direccion:"Av. America",telefono:424454)
Institucion.create(nombre:"Alemán Santa Maria",direccion:"Av. America",telefono:47242454)
Institucion.create(nombre:"Irlandés",direccion:"Av. America",telefono:4242454)
Institucion.create(nombre:"Loyola",direccion:"Av. America",telefono:4242454)
Institucion.create(nombre:"UCB",direccion:"Av. America",telefono:4242454)

User.create(nombre:"Prueba",apellido:"Perez",ci:123456,email:"rocha@api.com",password:"123456",password_confirmation:"123456",departamento_id:1,institucion_id:1,)

Rio.create(nombre:"Rio Rocha",cantidad_zonas:5,departamento_id:1)

ZonaRio.create(latitud:50,longitud:-70,rio_id:1)
